# CS371p: Object-Oriented Programming Allocator Repo

* Name: Marcus Mao

* EID: msm4343

* GitLab ID: marsunmao

* HackerRank ID: marcus_mao

* Git SHA: 9d6f97d2f95b2d10ed24301a012e1e83b8aa25f6 

* GitLab Pipelines: https://gitlab.com/marsunmao/cs371p-allocator/-/pipelines

* Estimated completion time: 15

* Actual completion time: 20

* Comments:  I was unable to finish the assignment due to starting way too late and having a lot of other things come up, so the "Actual completion time" is an estimate based on the little work/thinking I did get done.
