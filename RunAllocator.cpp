// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */

    // read
    string s;
    int t;
    getline(cin, s);
    t = stoi(s);
    assert(t <= 100);

    // skip blank line
    getline(cin, s);

    for(int i = 0; i < t; i++) {
        while(s.compare("")) {
            int request = stoi(s);
            assert(request >= -1000 && n <= 1000);

            // eval

            getline(sin, s);
        }
    }

    return 0;
}
